## Теоретические вопросы
1. Які існують типи даних у Javascript?
2. У чому різниця між == і ===?
3. Що таке оператор?

1. Які існують типи даних у Javascript?
   В JS існує вісім типів данних
   - Boolean - логічне значення типу правда/неправда, може бути лише true або false;
   - String - текстове значення, пишеться завжди в лапках (наприклад "апельсин");
   - Number - числове значення, пишеться без лапок звичайне число (наприклад 43);
   - BigInt - числове значення більше 2 в ступені 53. 
   - Null - по своїй суті пустота, може бути лише null;
   - Undefined - невідоме значення ;
   - Symbol - символьний тип, який є унікальним і його неможливо змінити. 
   - Object - об'єкт, масив, блок пам'яті або ж набір ключів чи значень

2. У чому різниця між == і ===?
 == використовується для порівняння двох змінних, але він не враховує тип даних змінної.
 === використовується для порівняння двох змінних, але він перевіряє тип даних і порівнює два значення.

3. Що таке оператор?
   У JS оператор - це спеціальний символ, що використовується для виконання операцій над значеннями та змінними.
   Оператори бувають:
   - оператори призначення: "=, +=, -=, *=, **=, ...";
   - aрифметичні оператори: " +, -, *, /";
   - oператори порівняння: "==, !=, ===, >, <, >=, <=";
   - лгічні оператори: "&, |, ^, ~, <<,  ...";
   - інші.
